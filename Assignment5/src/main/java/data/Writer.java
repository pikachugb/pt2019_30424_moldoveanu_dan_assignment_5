package data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Writer {
	private String fileName;
	private String content;
	
	public Writer(String fileName, String content) {
		this.fileName = fileName;
		this.content = content;
	}
	

	public void printFile() {
		String path = "";
		try {
			path = new File(".").getCanonicalPath() + "\\results";
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File file = new File(path + "\\" + fileName + ".txt");
		
	    BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
