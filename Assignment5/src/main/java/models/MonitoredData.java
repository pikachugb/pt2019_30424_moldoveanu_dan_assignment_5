package models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
	private Date startTime;
	private Date endTime;
	private String activity;
	
	public MonitoredData(Date startTime, Date endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	public Date getEndTime() {
		return endTime;
	}
	
	public String getActivity() {
		return activity;
	}
	
	@Override
	public String toString() {
		DateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return parser.format(startTime) + " | " + parser.format(endTime) + " | " + activity;
	}
	
	public Long getDurationInSeconds() {
		return (this.endTime.getTime() - this.startTime.getTime()) / 1000;
	}
	
	public Long getDurationInMinutes() {
		return (this.endTime.getTime() - this.startTime.getTime()) / 60000;
	}
}
