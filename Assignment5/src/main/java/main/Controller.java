package main;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;

import data.Writer;

import java.text.SimpleDateFormat;

import models.MonitoredData;
import models.Pair;

public class Controller {
	public static Writer writer;
	
	//1
	private static int calculateNumberOfDays(ArrayList<MonitoredData> monitoredDatas) {
		return (int) monitoredDatas.stream().map(a -> a.getStartTime().getDate()).distinct().count();
	}
	
	//2
	private static Map<String, Long> numberOfActivities(ArrayList<MonitoredData> monitoredDatas) {
		Map<String, Long> map = monitoredDatas.stream().collect(Collectors.groupingBy(n->n.getActivity(), Collectors.counting()));
		return map;
	}
	
	//3
	private static Map<String, Map<String, Integer>> activitiesPerDay(ArrayList<MonitoredData> monitoredDatas) {
		DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Map<String, Integer>> map = monitoredDatas.stream().collect(Collectors.groupingBy(sDate -> parser.format(sDate.getStartTime()), Collectors.groupingBy(activity -> activity.getActivity(), Collectors.summingInt(activity -> 1))));
		return map;
	}

	//4
	private static List<String> durationOfEachActivity(ArrayList<MonitoredData> monitoredDatas) { 
		List<String> map2 = (List<String>) monitoredDatas.stream().map(a -> a.getActivity() + " " + a.getDurationInMinutes()).collect(toList());
		return map2;
	}
	
	//5
	private static Map<Object, Long> entireDuration(ArrayList<MonitoredData> monitoredDatas) {
		return monitoredDatas.stream().map(a -> new Pair<String, Long>(a.getActivity(), a.getDurationInMinutes())).collect(Collectors.groupingBy( e -> e.getKey(), Collectors.summingLong(a -> a.getValue())));
	}
	
	//6
	private static boolean checkActivity(String activity, ArrayList<MonitoredData> monitoredDatas, Map<String, Long> numberOfActivities) {
		int current = 0;
		for (MonitoredData monitoredData: monitoredDatas) {
			int diffInMillies = (int) Math.abs(monitoredData.getEndTime().getTime() - monitoredData.getStartTime().getTime());
			int diff = (int) TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
			if (diff >= 5 && monitoredData.getActivity().equals(activity)) 
				current++;
		}
		if ((float)current/numberOfActivities.get(activity) > 0.9)
			 return false;
		return true;
	}	

	public static void main(String[] args) {
		String fileName = "Activities.txt";
		ArrayList<String> lines = new ArrayList<String>();
		ArrayList<MonitoredData> monitoredDatas = new ArrayList<MonitoredData>();
		//read file
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(lines::add);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//parse the txt
		for(String string: lines) {
			try {
				String[] p = string.split("		");
				Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(p[0]);
				Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(p[1]);
				MonitoredData newData = new MonitoredData(startTime, endTime, p[2].trim());
				monitoredDatas.add(newData);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		//calculate the number of days
		long totalDays = calculateNumberOfDays(monitoredDatas);
		System.out.println("Number of days monitored: " + totalDays);
		
		//how many time has an activity appeared during the monitoring period
		Map<String, Long> numberOfActivities = numberOfActivities(monitoredDatas);
		String numberofActivitiesString = "Number of activities during the monitoring period:\n" + numberOfActivities;
		writer = new Writer("NumberOfEachActivity", numberofActivitiesString);
		writer.printFile();
		
		//how many activities per day
		Map<String, Map<String, Integer>> activitiesPerDay = activitiesPerDay(monitoredDatas);
		String activitiesPerDayString = "";
		for (String s: activitiesPerDay.keySet()) {
			activitiesPerDayString += "Day " + s + ": ";
			activitiesPerDayString += activitiesPerDay.get(s) + "\n";
		}
		writer = new Writer("NumberOfActivitiesPerDay", activitiesPerDayString);
		writer.printFile();
		
		//the duration of each activity recorded
		List<String> durationOfEachActivityList = durationOfEachActivity(monitoredDatas);
		String durationOfEachActivityString = "";
		for( String s: durationOfEachActivityList) {
			durationOfEachActivityString += s + " minutes\n";
		}
		writer = new Writer("DurationOfEachActivity", durationOfEachActivityString);
		writer.printFile();
		
		//entire duration of each activity over the monitoring period: Leaving, Toileting, Showering, Sleeping, Breakfast, Lunch, Dinner, Snack, Spare_Time/TV, Grooming		
		String entireDurationString = entireDuration(monitoredDatas).toString();
		writer = new Writer("EachActivityEntirePeriod", entireDurationString);
		writer.printFile();
		
		//filter 90%
		String last = "";
		for (MonitoredData monitoredData: monitoredDatas) 
			if (checkActivity(monitoredData.getActivity(), monitoredDatas, numberOfActivities))
				last += monitoredData.toString() + "\n";
		writer = new Writer("FilteredActivies", last);
		writer.printFile();
	}

}
